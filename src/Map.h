

/**
 * potential to have a collection of players
 * @author stanglersm
 * @version 1.0
 * @created 17-Apr-2018 12:38:06 PM
 */
#ifndef SRC_OBJECT_H_
#define SRC_OBJECT_H_

#include "Room.h"
#include "Player.h"
#include <string>
#include <map>

class Map {
private:
    static constexpr int X_MAX = 6;
    static constexpr int Y_MAX = 6;
    static constexpr int NUM_FORKS = 2;
    static constexpr int NUM_TREASURES = 1;
    static constexpr int NUM_SWARMS = 2;
    static constexpr int NUM_LAVA = 3;
    static constexpr int NUM_EMPTY = (X_MAX * Y_MAX) - NUM_FORKS - NUM_TREASURES - NUM_SWARMS - NUM_LAVA - 1; // For the cake; player will replace nullptr
    // Num players and num cakes are both always 1, and will not have constants

public:
    Player* player;
    Room* rooms[X_MAX][Y_MAX];
	Map();

	// returns a string element with all of the messages from
	// adjacent elements
	std::string getNearby();

	// takes in char n, s, e, or w and moves one space in that direction
	// according to a compass with n being the top of the screen
	void playerAction(char c, bool isLunging);
	// each moveDirection function checks if the player is able to
	// move in that direction, and if the player can, it does
	std::string moveEast();
	std::string moveWest();
	std::string moveSouth();
	std::string moveNorth();
	std::string move(int x_modifier, int y_modifier);
	std::string display_map();


	// prints out the map
	void displayMap();
	void generateMap();
};

#endif /* SRC_OBJECT_H_ */
