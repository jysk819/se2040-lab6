/**
 * @author stanglersm
 * @version 1.0
 * @created 17-Apr-2018 12:38:06 PM
 */
#ifndef SRC_PLAYER_H_
#define SRC_PLAYER_H_

#include "Item.h"
#include <string>

class Player: public Item {
public:
    std::string enteredMessage;
    std::string nearbyMessage;
    char symbol;
    Player(int locationX, int locationY);
    int getX();
    int getY();
    void setX(int locationX);
    void setY(int locationY);
    int getNumWeapons();
    bool useWeapon();

private:
    int locationX;
    int locationY;
    int numWeapons;

};

#endif /* SRC_PLAYER_H_ */
