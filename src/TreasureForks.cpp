/*
 * TreasureForks.cpp
 *
 *  Created on: Apr 23, 2018
 *      Author: stanglersm
 */

#include "TreasureForks.h"

TreasureForks::TreasureForks() {
    enteredMessage = "A silverware drawer lies upon the ground.";
    nearbyMessage = "";
    symbol = '?';
}

int TreasureForks::valueInForks() {
    return 2;
}
