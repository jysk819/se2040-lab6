/*
 * Object.h
 *
 *  Created on: Apr 23, 2018
 *      Author: stanglersm
 */

#ifndef SRC_ITEM_H_
#define SRC_ITEM_H_

#include <string>

class Item {
public:
    Item();
    std::string enteredMessage;
    std::string nearbyMessage;
    char symbol;

    std::string getEnteredMessage();
    std::string getNearbyMessage();
    char getSymbol();
};

#endif /* SRC_ITEM_H_ */
