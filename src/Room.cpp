/*
 * Room.cpp
 *
 *  Created on: Apr 23, 2018
 *      Author: Jeremy Schwingbeck
 */

#include "Room.h"

Room::Room(Item* item) {
    setObject(item);
    // instantiate something
}

Item* Room::getObject() {
    return nullptr;
}

void Room::setObject(Item *item) {
    this->item = item;
}

void Room::vacate() {
    item = nullptr;
}

std::string Room::enter() {
    return nullptr;
}

bool Room::isOccupied() {
    if (item != nullptr) {
        return true;
    } else {
        return false;
    }
}

