/*
 * Object.cpp
 *
 *  Created on: Apr 23, 2018
 *      Author: stanglersm
 */

#include <string>
#include "Item.h"

Item::Item() {}

std::string Item::getEnteredMessage() {
    return enteredMessage;
}

std::string Item::getNearbyMessage() {
    return nearbyMessage;
}

char Item::getSymbol() {
    return symbol;
}
