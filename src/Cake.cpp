/*
 * Cake.cpp
 *
 *  Created on: Apr 23, 2018
 *      Author: stanglersm
 */

#include "Cake.h"

Cake::Cake() {
    enteredMessage =
            "You fell on the cake. The sprinkle swarm has overtaken you and the cake. Good job.";
    nearbyMessage = "A warm, chocolatey smell wafts through the air.";
    symbol = '!';
}
