/**
 * @author stanglersm
 * @version 1.0
 * @created 17-Apr-2018 12:38:06 PM
 */

#include "Weapon.h"
class SingleFork: public Weapon {
private:
    std::string enteredMessage;
    // the nearby message is an empty string for all weapons
    // because you should not know a weapon is near until you've found it.
    std::string nearbyMessage;
    char symbol;
public:
    SingleFork();
    // value in terms of number of forks represented by object
    int valueInForks();
};
