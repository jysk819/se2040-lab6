/**
 * @author stanglersm
 * @version 1.0
 * @created 17-Apr-2018 12:38:06 PM
 */
#ifndef SRC_WEAPON_H_
#define SRC_WEAPON_H_

#include <string>
#include "Item.h"

class Weapon: public Item {
private:
    std::string enteredMessage;
    std::string nearbyMessage;
    char symbol;

public:
    // the constructor for weapon and its children
    // define all of the instance variables, hard coded
    Weapon();
    // :baseclass and then name the paramters
    // look at the MilTime example

    // returns the number of weapons picked up in this room
    virtual int valueInForks() = 0;
};

#endif /* SRC_WEAPON_H_ */
