/**
 * can try to eat, have to pass number of forks, and if it's greater than zero,
 * eat() returns true. If you don't have any forks, it returns false.
 * @author stanglersm
 * @version 1.0
 * @created 17-Apr-2018 12:38:06 PM
 */
#include <string>
#include "Item.h"

class Cake: public Item {
public:
    Cake();

private:
    std::string enteredMessage;
    std::string nearbyMessage;
    char symbol;
};
