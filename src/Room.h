
/**
 * empty room is (.) - make a default constructor with no parameters for an empty
 * room
 * @author Jeremy Schwingbeck
 * @version 1.0
 * @created 17-Apr-2018 12:38:06 PM
 */

#include "Item.h"

class Room {
private:
    Item *item;
public:
    Room(Item* item = nullptr);
    Item* getObject();
    void setObject(Item* item);
    void vacate();
    std::string enter();
    bool isOccupied();
};
