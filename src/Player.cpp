/*
 * Player.cpp
 *
 *  Created on: Apr 23, 2018
 *      Author: stanglersm
 */

#include "Player.h"

Player::Player(int locationX, int locationY) {
    setX(locationX);
    setY(locationY);
}

int Player::getX() {
    return locationX;
}
int Player::getY() {
    return locationY;
}
void Player::setX(int locationX) {
    this->locationX = locationX;
}
void Player::setY(int locationY) {
    this->locationY = locationY;
}
int Player::getNumWeapons() {
    return numWeapons;
}
bool Player::useWeapon() {
    if (numWeapons > 0) {
        numWeapons--;
        return true;
    }
    return false;
}
