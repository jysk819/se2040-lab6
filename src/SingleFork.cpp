/*
 * SingleFork.cpp
 *
 *  Created on: Apr 23, 2018
 *      Author: stanglersm
 */

#include "SingleFork.h"

SingleFork::SingleFork() {
    enteredMessage = "You find a clean fork on the ground.";
    nearbyMessage = "";
    symbol = '-';
}

int SingleFork::valueInForks() {
    return 1;
}
