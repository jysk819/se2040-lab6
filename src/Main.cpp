/*
 * Main.cpp
 *
 *  Created on: Apr 23, 2018
 *      Author: schwingbeckjj
 */

#include "Map.h"
#include <iostream>
#include <locale>
using namespace std;

class Main {
public:
    int main(int argc, char **argv) {
        string output_msg =
                "You enter the Cave o' Cake in search of delectable cake! What will you do?";
        string options_msg =
                "N)orth, S)outh, E)ast, W)est, L)unge, or D)isplay map?";
        string input = "";
        bool is_lunging = false;
        locale loc;
        char c;
        Map* map = new Map();
        map->generateMap();

        while (true) { // Operations in subclasses will have system exiting
            cout << output_msg << options_msg << endl;
            cin >> input;
            c = tolower(input[0], loc);
            switch (tolower(input[0], loc)) {
            case 'n':
            case 'e':
            case 's':
            case 'w':
                map->playerAction(c, false);
                break;
            case 'd':
                map->displayMap();
                break;
            case 'l':
                cout << "What direction? N)orth, S)outh, E)ast or W)est?";
                cin >> input;
                c = tolower(input[0], loc);
                // Nested switch for if lunging
                switch (c) {
                case 'n':
                case 'e':
                case 's':
                case 'w':
                    map->playerAction(c, true);
                    break;
                default:
                    cout << "Invalid direction, returning to top menu";
                    break;
                } // End of nested switch
                break;
            default:
                cout << "Invalid direction, returning to top menu";
                break;
            }
        }
    }

};
