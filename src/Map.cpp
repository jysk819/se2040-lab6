/*
 * Map.cpp
 *
 *  Created on: Apr 23, 2018
 *      Author: schwingbeckjj
 */

#include "Map.h"
#include "Cake.h"
#include "SingleFork.h"
#include "TreasureForks.h"
#include "Player.h"
#include "StickyLava.h"
#include "SprinkleSwarm.h"

#include <map>
#include <cstdlib>
#include <iostream>
#include <type_traits>
#include <algorithm>    // std::random_shuffle
#include <vector>

using namespace std;

Map::Map() {
    generateMap();
    displayMap();
}

std::string Map::getNearby() {
    return nullptr;
}

void Map::displayMap() {
    cout << "+------+" << endl;
    int count = 0;
//    for (Room r : rooms) {
//        if (r.isOccupied()) {
//            cout << r.getObject().getSymbol();
//        } else {
//            cout << '.';
//        }
//    }
    cout << endl;
    cout << "+------+";
}

void Map::generateMap() {
//    std::map<int, int> occupiedCells;
//
//    Player *player = new Player(X_MAX, Y_MAX);
//    rooms[0][0] = *player;
//
//    Cake *cake = new Cake();
//    rooms[5][5] = *cake;
//
//    SingleFork *sf1 = new SingleFork();
//    SingleFork *sf2 = new SingleFork();
//    rooms[1][1] = *sf1;
//    rooms[2][2] = *sf2;

    vector<Item*> itemsVector;
    for (int i = 0; i < NUM_FORKS; i++) {
        SingleFork *fork = new SingleFork();
        itemsVector.push_back(fork);
    }
    for (int i = 0; i < NUM_LAVA; i++) {
        StickyLava *lava = new StickyLava();
        itemsVector.push_back(lava);
    }
    for (int i = 0; i < NUM_SWARMS; i++) {
        SprinkleSwarm *swarm = new SprinkleSwarm();
        itemsVector.push_back(swarm);
    }
    for (int i = 0; i < NUM_TREASURES; i++) {
        TreasureForks *treasure = new TreasureForks();
        itemsVector.push_back(treasure);
    }
    Cake *cake = new Cake();
    random_shuffle(itemsVector.begin(), itemsVector.end());

    int k = 0;
    for (int j = 0; j < Y_MAX; j++) {
        for (int i = 0; i < X_MAX; i++) {
            Room *room = new Room(itemsVector[k]);
            rooms[i][j] = room;
            k++;
        }
    }
}

//std::map<int, int> addObject(Object[] objects, std::map<int, int> occupiedCells) {
//    for (Object o : objects) {
//        bool objPlaced = false;
//        while (!objPlaced) {
//            //    int randomX = (rand() % 6); // produces a random number between 0 and 5, inclusive
//    int randomY = (rand() % 6); // can add 1 to this value should we desire it to be 1 and 6 instead
//            if (!(occupiedCells[randomX] == randomY)) {
//                rooms[x]
//                occupiedCells[randomX] = randomY;
//                objPlaced = true;
//            }
//        }
//    }
//
//    return occupiedCells;
//}

void Map::playerAction(char c, bool isLunging) {
    string output = "";
    switch (c) {
    case 'e':
        moveEast();
        break;
    case 'w':
        moveWest();
        break;
    case 's':
        moveSouth();
        break;
    case 'n':
        moveNorth();
        break;
    default: // given an invalid direction
        std::cout << "Invalid direction - I will stay put\n";
        break;
    }
}

string Map::moveEast() {
// check if we can move East
    if (player->getX() < X_MAX - 1) {
        return move(1, 0);
    } else {
        return "Invalid move: East";
    }
}

string Map::moveWest() {
// check if we can move West
    if (player->getX() > 0) {
        return move(-1, 0);
    } else {
        return "Invalid move: West";
    }
}

string Map::moveSouth() {
// check if we can move South
    if (player->getY() < Y_MAX - 1) {
        return move(0, 1);
    } else {
        return "Invalid move: South";
    }
}

string Map::moveNorth() {
// check if we can move North
    if (player->getY() > 0) {
        return move(0, -1);
    } else {
        return "Invalid move: North";
    }
}

string Map::move(int x_modifier, int y_modifier) {
//    ((Room) rooms[player.locationX][player.locationY]).vacate();
//            player.locationX += x_modifier;
//            player.locationY += y_modifier;
//            if (std::is_base_of<Hazard, rooms[player.locationX][player.locationY].class>::value) {
//                ((Hazard) rooms[player.locationX][player.locationY]).attack(&player);
//            }
//            ((Room) rooms[player.locationX][player.locationY]).enter();
//            return ((Room) rooms[player.locationX][player.locationY]).getObject().enteredMessage;
    return "";
}
