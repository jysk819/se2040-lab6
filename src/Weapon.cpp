/*
 * Weapon.cpp
 *
 *  Created on: Apr 23, 2018
 *      Author: stanglersm
 */

#include "Weapon.h"

Weapon::Weapon() {
    enteredMessage = "You've found a weapon.";
    nearbyMessage = "";
    symbol = '-';
}
