/**
 * @author stanglersm
 * @version 1.0
 * @created 17-Apr-2018 12:38:06 PM
 */

#include "Weapon.h"

class TreasureForks: public Weapon {
private:
    std::string enteredMessage;
    std::string nearbyMessage;
    char symbol;
public:
    TreasureForks();
    // returns the number of weapons picked up in this room
    int valueInForks();
};
