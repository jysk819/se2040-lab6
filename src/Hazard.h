

/**
 * @author stanglersm
 * @version 1.0
 * @created 17-Apr-2018 12:38:06 PM
 */
#include "Player.h"
#include "Item.h"
#include <string>

#ifndef SRC_HAZARD_H_
#define SRC_HAZARD_H_

class Hazard : public Item {
public:
	Hazard();

	virtual void attack(Player *player);

	std::string getEnteredMessage();
	std::string getNearbyMessage();

    std::string enteredMessage;
    std::string nearbyMessage;
    char symbol;
};

#endif /* SRC_HAZARD_H_ */
