
/**
 * @author stanglersm
 * @version 1.0
 * @created 17-Apr-2018 12:38:06 PM
 */

#include "Hazard.h"
#include "Player.h"
#include <string>

class SprinkleSwarm: public Hazard {

public:
    SprinkleSwarm() { }

    /*virtual*/ void attack(Player *player);

    std::string getEnteredMessage();
    std::string getNearbyMessage();

    std::string enteredMessage =
            "As the swarm of sprinkles flies past, you realize the cake is no longer pure. They have won, you have not.";
    std::string nearbyMessage = "You hear the sound of sprinkles fluttering about nearby";
    char symbol;
};
