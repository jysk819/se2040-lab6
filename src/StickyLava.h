

/**
 * @author stanglersm
 * @version 1.0
 * @created 17-Apr-2018 12:38:06 PM
 */

#include "Hazard.h"

class StickyLava : public Hazard {

public:
	StickyLava() {}

	virtual void attack(Player *player);

    std::string getEnteredMessage();
    std::string getNearbyMessage();

    std::string enteredMessage;
    std::string nearbyMessage;
    char symbol;
};
